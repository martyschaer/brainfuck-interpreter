# Brainfuck Interpreter
A small Brainfuck interpreter written in Java.

## How to
Takes two arguments:

1) the brainfuck program.
2) the input to the bf program.

## Brainfuck
Learn all about brainfuck on [Wikipedia](https://en.wikipedia.org/wiki/Brainfuck).
