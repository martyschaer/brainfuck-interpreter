import java.util.Objects;
import java.util.stream.Stream;

public class Parser {
    private Command[] commands;

    public Command[] parse(final String input){
        Character[] filtered = filter(input);
        return Stream.of(filtered)
                .filter(o -> !Objects.isNull(o))
                .map(Command::of)
                .toArray(Command[]::new);
    }

    private Character[] filter(String input){
        Character[] out = new Character[input.length()];
        int i = 0;
        for(char c : input.toCharArray()){
            if( c == '>' ||
                c == '<' ||
                c == '+' ||
                c == '-' ||
                c == '.' ||
                c == ',' ||
                c == '[' ||
                c == ']'){
                out[i++] = c;
            }
        }
        return out;
    }
}
