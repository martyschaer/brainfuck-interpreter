public enum Command {
    INC('>'),
    DEC('<'),
    ADD('+'),
    SUB('-'),
    OUT('.'),
    INP(','),
    START('['),
    END(']');

    private Character cmd;

    Command(Character command){
        this.cmd = command;
    }

    public static Command of(Character c){
        for(Command cmd : Command.values()){
            if(cmd.cmd == c){
                return cmd;
            }
        }
        throw new IllegalArgumentException(c + " is not known!");
    }
}
