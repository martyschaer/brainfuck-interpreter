import programs.Numwarp;
import programs.Program;

public class Main {

    public static void main(String[] args){
        Parser parser = new Parser();

        if(args.length != 2){
            System.out.println("Please only provide your brainfuck program and input as a string");
            System.out.println("Running an example program:");
            Program numwarp = new Numwarp();
            System.out.println(numwarp.getPrintableInfo());
            Command[] commands = parser.parse(numwarp.getProgram());
            Input input = new Input(numwarp.getInput());

            Machine machine = new Machine(input);

            machine.execute(commands);

            System.exit(1);
        }

        Command[] commands = parser.parse(args[0]);

        Input input = new Input(args[1]);

        Machine machine = new Machine(input);

        machine.execute(commands);

        machine.reset();
    }
}
