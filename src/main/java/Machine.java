import java.util.EmptyStackException;
import java.util.Stack;

public class Machine {
    private int instructionPointer = 0;
    private Stack<Integer> jumpPointers = new Stack<>();
    private Command[] commands;
    private Input input;
    private State state = new State();

    public Machine(Input input){
        this.input = input;
    }

    public Machine(){

    }

    public void reset(){
        this.instructionPointer = 0;
        this.jumpPointers.clear();
        this.commands = new Command[0];
        this.input.reset();
        this.state.reset();
    }

    public void setInput(Input input){
        this.input = input;
    }

    public void execute(Command[] cmds){
        this.commands = cmds;
        System.out.printf("Executing % d commands..%n", commands.length);
        long start = System.nanoTime();
        while(instructionPointer < commands.length){
            Command cmd = commands[instructionPointer];

            switch (cmd){
                case INC:
                    state.incrementPointer();
                    break;
                case DEC:
                    state.decrementPointer();
                    break;
                case ADD:
                    state.incrementData();
                    break;
                case SUB:
                    state.decrementData();
                    break;
                case START:
                    if(state.isZero()){
                        // if the current data byte is zero
                        int encountered = 0;
                        do{
                            instructionPointer++;
                            cmd = commands[instructionPointer];
                            if(cmd.equals(Command.START)){
                                encountered++;
                            }else if(cmd.equals(Command.END)){
                                encountered--;
                            }
                        }while(!cmd.equals(Command.END) || encountered >= 0);
                        // jump ahead to the next END command
                    }else{
                        //otherwise, push the start onto the stack
                        jumpPointers.push(instructionPointer);
                    }
                    break;
                case END:
                    if(state.isNonZero()){
                        // if the current data byte is not zero
                        // reset the IP to whatever is last on the stack
                        instructionPointer = jumpPointers.peek();
                    }else{
                        // otherwise, remove the last START command from the stack
                        jumpPointers.pop();
                    }
                    break;
                case OUT:
                    if(state.isPrintable()){
                        // if it's printable, print it
                        System.out.print(Character.valueOf((char)state.get()));
                    }else{
                        // otherwise print out hex value
                        System.out.printf("[0x%02X]", state.get());
                    }
                    break;
                case INP:
                    // set the current byte to whatever is next in the input
                    state.set(input.get());
                    break;
            }
            instructionPointer++;
        }
        long end = System.nanoTime();
        double ms = (end-start) / 1_000_000.0;
        System.out.printf("Executed in %f ms%n", ms);
    }
}
