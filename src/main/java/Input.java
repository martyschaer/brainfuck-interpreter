import java.nio.charset.StandardCharsets;

public class Input {
    private byte[] input;
    private int pointer;

    public Input(String input){
        this.input = input.getBytes(StandardCharsets.UTF_8);
    }
    
    public byte get(){
        if(pointer < input.length){
            return input[pointer++];
        }
        return (byte)0; //zero termination
    }

    public void reset(){
        this.pointer = 0;
    }
}
