package programs;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Numwarp by
 * Daniel B Cristofani (cristofdathevanetdotcom)
 * http://www.hevanet.com/cristofd/brainfuck/
 */
public class Numwarp implements Program{

    @Override
    public String getProgramName() {
        return "NumWarp";
    }

    @Override
    public String getAuthorInfo() {
        return "Daniel B Cristofani (cristofdathevanetdotcom)";
    }

    @Override
    public String getProgram() {
        return ">>>>+>+++>+++>>>>>+++[\n" +
            "  >,+>++++[>++++<-]>[<<[-[->]]>[<]>-]<<[\n" +
            "    >+>+>>+>+[<<<<]<+>>[+<]<[>]>+[[>>>]>>+[<<<<]>-]+<+>>>-[\n" +
            "      <<+[>]>>+<<<+<+<--------[\n" +
            "        <<-<<+[>]>+<<-<<-[\n" +
            "          <<<+<-[>>]<-<-<<<-<----[\n" +
            "            <<<->>>>+<-[\n" +
            "              <<<+[>]>+<<+<-<-[\n" +
            "                <<+<-<+[>>]<+<<<<+<-[\n" +
            "                  <<-[>]>>-<<<-<-<-[\n" +
            "                    <<<+<-[>>]<+<<<+<+<-[\n" +
            "                      <<<<+[>]<-<<-[\n" +
            "                        <<+[>]>>-<<<<-<-[\n" +
            "                          >>>>>+<-<<<+<-[\n" +
            "                            >>+<<-[\n" +
            "                              <<-<-[>]>+<<-<-<-[\n" +
            "                                <<+<+[>]<+<+<-[\n" +
            "                                  >>-<-<-[\n" +
            "                                    <<-[>]<+<++++[<-------->-]++<[\n" +
            "                                      <<+[>]>>-<-<<<<-[\n" +
            "                                        <<-<<->>>>-[\n" +
            "                                          <<<<+[>]>+<<<<-[\n" +
            "                                            <<+<<-[>>]<+<<<<<-[\n" +
            "                                              >>>>-<<<-<-\n" +
            "  ]]]]]]]]]]]]]]]]]]]]]]>[>[[[<<<<]>+>>[>>>>>]<-]<]>>>+>>>>>>>+>]<\n" +
            "]<[-]<<<<<<<++<+++<+++[\n" +
            "  [>]>>>>>>++++++++[<<++++>++++++>-]<-<<[-[<+>>.<-]]<<<<[\n" +
            "    -[-[>+<-]>]>>>>>[.[>]]<<[<+>-]>>>[<<++[<+>--]>>-]\n" +
            "    <<[->+<[<++>-]]<<<[<+>-]<<<<\n" +
            "  ]>>+>>>--[<+>---]<.>>[[-]<<]<\n" +
            "]";
    }

    @Override
    public String getInput() {
        return LocalDate.now().format(DateTimeFormatter.ISO_DATE);
    }

    @Override
    public String getUrlInfo(){
        return "http://www.hevanet.com/cristofd/brainfuck/";
    }
}
