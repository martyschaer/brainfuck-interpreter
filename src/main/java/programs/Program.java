package programs;

public interface Program {
    String getProgramName();
    String getAuthorInfo();
    String getUrlInfo();
    String getProgram();
    String getInput();
    default String getPrintableInfo(){
        return String.format("Running: %s by %s%navailable at: %s%nwith input: {%s}%n", getProgramName(), getAuthorInfo(), getUrlInfo(), getInput());
    }
}
