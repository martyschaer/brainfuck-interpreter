import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class State {

    private Byte[] data = new Byte[100_000];
    private int pointer = data.length / 2;

    public State(){
        this.reset();
    }

    public void reset(){
        this.pointer = data.length / 2;
        Arrays.fill(data, (byte)0);
    }

    public void incrementPointer(){
        pointer++;
    }

    public void decrementPointer(){
        pointer--;
    }

    public byte get(){
        return data[pointer];
    }

    public void set(byte b){
        data[pointer] = b;
    }

    public void incrementData(){
        data[pointer]++;
    }

    public void decrementData(){
        data[pointer]--;
    }

    public boolean isZero(){
        return get() == 0;
    }

    public boolean isNonZero(){
        return !isZero();
    }

    public boolean isPrintable(){
        return (0x20 <= get() && get() <= 0x7E) ||
                0x07 <= get() && get() <= 0x0D;
    }

    /**
     * Dumps out all non-zero cells currently in the state.
     */
    public String dump(){
        return Stream.of(data)
                .filter(b -> b != 0)
                .map(b -> String.format("[0x%02X]", b))
                .collect(Collectors.joining());
    }
}
